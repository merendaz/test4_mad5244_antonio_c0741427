//
//  GameScene.swift
//  BabaIsYou-F19
//
//  Created by Parrot on 2019-10-17.
//  Copyright © 2019 Parrot. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {

    // MARK: Outlets for sprites
    // Things
    var player:SKSpriteNode!
    var flag:SKSpriteNode!
    var wall:SKSpriteNode! //FIXED
    
    //Blocks:
    var isblock1:SKSpriteNode! //FIXED
    var isblock2:SKSpriteNode! //FIXED
    var flagblock:SKSpriteNode!
    var wallblock:SKSpriteNode!
    var winblock:SKSpriteNode!
    var stopblock:SKSpriteNode!
    
    //Label:
    var congratlbl:SKLabelNode!
    
    
    let PLAYER_SPEED:CGFloat = 32
    
    var isStop = false
    var isWin = false
    
    var wallblockIs = false
    var flagblockIs = false
    
    override func didMove(to view: SKView) {
        self.physicsWorld.contactDelegate = self
        // Initialize the player and the other sprites
        self.player = self.childNode(withName: "baba") as? SKSpriteNode
        self.flag = self.childNode(withName: "flag") as? SKSpriteNode
        self.wall = self.childNode(withName: "wall") as? SKSpriteNode
        self.isblock1 = self.childNode(withName: "isblock1") as? SKSpriteNode
        self.isblock2 = self.childNode(withName: "isblock2") as? SKSpriteNode
        self.flagblock = self.childNode(withName: "flagblock") as? SKSpriteNode
        self.wallblock = self.childNode(withName: "wallblock") as? SKSpriteNode
        self.winblock = self.childNode(withName: "winblock") as? SKSpriteNode
        self.stopblock = self.childNode(withName: "stopblock") as? SKSpriteNode
        self.congratlbl = self.childNode(withName: "congratlbl") as? SKLabelNode
        self.congratlbl.text = ""
    }
    
    override func update(_ currentTime: TimeInterval) {
        self.isStop = self.alignOrd(left: isblock1, right: stopblock)
        self.isWin = self.alignOrd(left: isblock2, right: winblock)
        
        self.wallblockIs = self.alignOrd(left: wallblock, right: isblock1)
        self.flagblockIs = self.alignOrd(left: flagblock, right: isblock2)
        
        if (self.wallblockIs && self.isStop) {
            self.player.physicsBody?.collisionBitMask = 254
            self.wall.physicsBody?.collisionBitMask = 4294967295
        }
        else {
            self.player.physicsBody?.collisionBitMask = 250
            self.wall.physicsBody?.collisionBitMask = 0
        }
        if ((self.flagblockIs && self.isWin) && (self.player.frame.intersects(self.flag.frame))) {
            self.congratlbl.text = "Congratulations!"
        }
    }
    
    // ============================================================================
    // TOUCHESBEGAN ===============================================================
    // ============================================================================
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // GET THE POSITION WHERE THE MOUSE WAS CLICKED
        // ---------------------------------------------
        let mouseTouch = touches.first
        if (mouseTouch == nil) {
            return
        }
        let location = mouseTouch!.location(in: self)
        
        // WHAT NODE DID THE PLAYER TOUCH
        // ----------------------------------------------
        let nodeTouched = atPoint(location).name
        
        // GAME LOGIC: Move player based on touch
        if (nodeTouched == "upButton") {
            // move up
            self.player.position.y = self.player.position.y + PLAYER_SPEED
        }
        else if (nodeTouched == "downButton") {
            // move down
            self.player.position.y = self.player.position.y - PLAYER_SPEED
        }
        else if (nodeTouched == "leftButton") {
            // move left
            self.player.position.x = self.player.position.x - PLAYER_SPEED
        }
        else if (nodeTouched == "rightButton") {
            // move right
            self.player.position.x = self.player.position.x + PLAYER_SPEED
        }
    }
    // ============================================================================
    // ============================================================================
    
    func alignOrd (left: SKSpriteNode, right: SKSpriteNode) -> Bool {
        if (((self.between(n: Int(left.position.y), min:(Int(right.position.y)-5) , max: Int(right.position.y)+5)) && (left.position.x < right.position.x))) {
            return true
        }
        else {
            return false
        }
    }
    
    func between (n: Int, min: Int, max: Int) -> Bool {
        if (min ... max).contains(n) {
            return true
        }
        else {
            return false
        }
    }
}
